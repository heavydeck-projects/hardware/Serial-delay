# This qmake .pro file is intended for XP builds using Qt 5.6.3 with MinGW 4.9.2
# It may not work on more modern Qt releases, use CMake instead.
QT += core serialport
QT -= gui

CONFIG += gnu++11

TARGET = serial-latency
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp
win32:RC_ICONS += icon.ico
